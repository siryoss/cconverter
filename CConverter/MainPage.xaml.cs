﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace CConverter
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //private DataModel dataModel;
        //public string CurrencyOneCode { get; set; };
        //public string CurrencyTwoCode { get; set; };


        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            var mainPageModel =  new MainPageModel();
            this.DataContext = mainPageModel;
            mainPageModel.CurrencyOneLabel = "PLN";
            mainPageModel.CurrencyTwoLabel = "IDR";
            mainPageModel.CurrencyOneFX = 3507;
            mainPageModel.CurrencyTwoFX = 0.000285;
            mainPageModel.CurrencyOneAmount = 10;
            mainPageModel.CurrencyTwoAmount = 10000;
            //dataModel = new DataModel();

            //currencyOneName = "PLN";
            //currencyTwoName = "IDR";
            //currencyOneFX = 3507;
            //currencyTwoFX = 0.000285;
            //updateLabels();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }
    }
}
